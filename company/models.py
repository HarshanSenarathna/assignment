from django.db import models

# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    logo = models.ImageField(upload_to='pics')
    website = models.CharField(max_length=100)