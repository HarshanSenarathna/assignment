from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.login, name='index'),
    path('actionLogin', views.actionLogin, name='index'),
    # path('deleteEmployee/<int:id>', views.deleteEmployee, name='employee_list'),
    # path('media', views.saveCompany, name='company_list')
]

