from django.shortcuts import render, redirect
from .models import User
# Create your views here.


def login(request):
    return render(request, 'home.html')


def actionLogin(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']

        try:
            company = User.objects.get(email=email, password=password)

            if company:
                return redirect('/employee')

            else:
                return render(request, 'home.html')
        except:
            return render(request, 'home.html')
    else:
        return render(request, 'home.html')


