from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('employee', views.EmployeeList.as_view(), name='employee_list'),
    path('saveEmployee', views.saveEmployee, name='employee_list'),
    path('deleteEmployee/<int:id>', views.deleteEmployee, name='employee_list'),
    # path('media', views.saveCompany, name='company_list')
]
