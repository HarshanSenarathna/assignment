from django.db import models

# Create your models here.


class Employee(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.EmailField()
    company = models.ForeignKey(
        'company.Company', on_delete=models.CASCADE)
    email = models.CharField(max_length=100)