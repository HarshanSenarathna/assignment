from django.shortcuts import render, redirect
from .models import Employee
from company.models import Company
from django.views.generic import ListView

# Create your views here.


def saveEmployee(request):
    if request.method == "POST":
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        companyid = request.POST['company']
        email = request.POST['email']

        company = Company.objects.get(id=companyid)

        if firstname and lastname and company and email:
            employee = Employee.objects.create(
                firstname=firstname,
                lastname=lastname,
                company=company,
                email=email
            )
            employee.save()

    return redirect('/employee')


class EmployeeList(ListView):
    model = Employee


def deleteEmployee(request, id):
    instance = Employee.objects.get(id=id)
    instance.delete()
    return redirect('/employee')
